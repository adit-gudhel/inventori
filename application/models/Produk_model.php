<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk_model extends CI_Model {

	protected $table = 'produk';

	function getAll($limit, $offset){
		// SELECT * FROM produk;

		$this->db->select('produk_id, produk, stok, foto, kategori, supplier')
			->join('kategori k', 'p.kategori_id = k.kategori_id')
			->join('supplier s', 'p.supplier_id = s.supplier_id')
			->order_by('produk_id', 'asc');
		return $this->db->get($this->table . ' p', $limit, $offset);
	}

	function simpan($data){
		$this->db->insert($this->table, $data);
	}

	function perbaharui($data, $key){
		$this->db->update($this->table, $data, $key);
	}

	function hapus($id){
		$this->db->delete($this->table, array('produk_id' => $id));
	}

	function total(){
		return $this->db->count_all('produk');
	}

}