<?php
defined('BASEPATH') OR exit('No direct script access allowed');

Use Faker\Factory;

class Welcome extends CI_Controller {

	public function index(){
		$this->load->view('welcome_message');
	}

}
