<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk extends CI_Controller {

	protected $per_page;

	public function __construct(){
		parent::__construct();

		$this->load->model('produk_model', 'mdl');
		$this->load->library('form_validation');
		$this->load->helper('form');

		$this->per_page = 5;
	}

	public function index(){
		// Membuat paginasi
		$this->load->library('pagination');

		$config['base_url'] 	= base_url('produk/index');
		$config['total_rows'] 	= $this->mdl->total();
		$config['per_page'] 	= $this->per_page;

		$this->pagination->initialize($config);	

		$data['title'] 		=  'Daftar Produk';
		$data['products'] 	=  $this->mdl->getAll($this->per_page, $this->uri->segment(3))->result_array();

		$this->load->view('produk/index', $data);
	}

	public function tambah(){
		
		$this->load->model('kategori_model', 'kategori');
		$this->load->model('supplier_model', 'supplier');

		$data['title'] 		=  'Form Tambah Produk Baru';
		$data['suppliers']	= $this->supplier->getAll()->result();
		$data['categories']	= $this->kategori->getAll()->result_array();

		// echo '<pre>'; print_r($data); exit;

		$this->load->view('produk/create', $data);
	}

	public function ubah(){
		$id 					= $this->uri->segment(3);

		// cek apakah id yg di passing di url ada di database>
		$cek 					= $this->db->get_where('produk', array('produk_id' => $id));

		if(!empty($id) && $cek->num_rows() > 0) :
			
			$this->load->model('kategori_model', 'kategori');
			$this->load->model('supplier_model', 'supplier');
			
			// ada
			$data['title'] 		=  'Form Edit Produk';
			$data['product'] 	=  $cek->row_array();
			$data['suppliers']	= $this->supplier->getAll()->result();
			$data['categories']	= $this->kategori->getAll()->result_array();

			// echo '<pre>'; print_r($data); exit;

			$this->load->view('produk/edit', $data);
		else :
			// tidak ada 
			show_404();
		endif;
	}

	public function hapus($id){
		$this->mdl->hapus($id);

		redirect('produk');
	}

	public function save(){
		$id 					= $this->input->post('produk_id', TRUE);

		// form validasi
        $val = $this->form_validation;

       	// atur rule validasi
       	$val->set_rules('produk', 'Nama Produk', 'required|max_length[100]');
       	$val->set_rules('deskripsi', 'Deskripsi Produk', 'required');
       	$val->set_rules('stok', 'Stok Produk', 'required|numeric|max_length[3]');
       	$val->set_rules('supplier_id', 'Pemasok', 'required');
       	$val->set_rules('kategori_id', 'Kategori Produk', 'required');

        if ($val->run() == FALSE) :
           
           if(empty($id)) :
           		$this->tambah();
           else :
           		$this->edit();
           endif;
        
        else :
        	// lolos validasi
			$data['produk'] 		= $this->input->post('produk', TRUE);
			$data['deskripsi'] 		= $this->input->post('deskripsi', TRUE);
			$data['stok'] 			= $this->input->post('stok', TRUE);
			$data['supplier_id'] 	= $this->input->post('supplier_id', TRUE);
			$data['kategori_id'] 	= $this->input->post('kategori_id', TRUE);


			// Cek apakah ada file upload
			if(!empty($_FILES['foto']['name'])) :
				$upload = $this->_upload_foto();

				if($upload['error'] == TRUE) :
					 echo '<pre>'; print_r($upload['message']); exit;
				else :
					$data['foto'] = $upload['data']['file_name'];
				endif;
			endif;

			// Cek apakah insert atau update database
			if(!empty($id)) :
				// Update database
				$cek = $this->db->get_where('produk', array('produk_id' => $id));

				if($cek->num_rows() > 0) :
					$this->mdl->perbaharui($data, array('produk_id' => $id));
				else :
					// tampilkan error
					show_404();
				endif;

			else :
				// Insert database
				$this->mdl->simpan($data);
			endif;

			redirect('produk');

		endif;
	}

	private function _upload_foto(){
		$config['upload_path']          = './assets/images/';
        $config['allowed_types']        = 'jpg|png|jpeg';
        $config['max_size']             = 2048; // in Kb -> 2Mb

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('foto')){
        	$result['error'] 	= TRUE;
        	$result['message'] 	= $this->upload->display_errors();
            
        } else {
        	$result['error'] 	= FALSE;
			$result['data'] 	= $this->upload->data();
        }

        return $result;
	}

}