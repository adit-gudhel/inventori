<?php
defined('BASEPATH') OR exit('No direct script access allowed');

Use Faker\Factory;

class Seeder extends CI_Controller {

	// Sebelum masuk method / function pada controller constructor selalu dieksekusi terlebih dahulu
	function __construct(){
		parent::__construct();

		$this->load->model('seeder_model', 'mdl');

		// $this->mdl atau $this->seeder_model
	}

	// Jika pengguna mengakses controller tanpa nama function / method maka default function yg di panggil adalah index
	public function index(){
		
		$faker = Faker\Factory::create('id_ID');

		$result = array();

		for($i=1; $i <= 100; $i++) :

			// $data = array(
			// 	'produk' 		=> $faker->sentence(3),
			// 	'deskripsi' 	=> $faker->sentence(100),
			// 	'stok' 			=> $faker->randomNumber(3),
			// 	'supplier_id' 	=> $faker->randomElement($array = array(1,2)),
			// 	'kategori_id' 	=> $faker->randomElement($array = array(1,2,3,4))
			// );

			// alternatif
			// $data['produk'] 		= $faker->sentence(3);
			// $data['deskripsi'] 		= $faker->sentence(100);
			// $data['stok'] 			= $faker->randomNumber(3);
			// $data['supplier_id'] 	= $faker->randomElement($array = array(1,2));
			// $data['kategori_id'] 	= $faker->randomElement($array = array(1,2,3,4));

			$data = [
				'produk' 		=> $faker->sentence(3),
				'deskripsi' 	=> $faker->sentence(100),
				'stok' 			=> $faker->randomNumber(3),
				'supplier_id' 	=> $faker->randomElement($array = array(1,2)),
				'kategori_id' 	=> $faker->randomElement($array = array(1,2,3,4))
			];


			array_push($result, $data); 

			// insert ke tabel produk

			$this->mdl->simpan($data);

		endfor;

		echo '<pre>'; print_r($result); exit;



		// echo '<ul>';
		// for($i=1; $i <= 100; $i++) :
		// 	echo '<li>' . $faker->sentence(100) . '</li>';
		// endfor;
		// echo '</ul>';
	}
}