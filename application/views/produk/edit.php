<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?= $title; ?></title>

	<?php $this->load->view('template/styles'); ?>

</head>
<body>

<div id="container">
	
	<h1><?php echo $title; ?></h1>

	<div id="body">

		<code><button type="button" onclick="window.location.href = '<?= base_url('produk'); ?>';">Kembali ke Daftar</button></code>

		<?php echo validation_errors(); ?>

		<?= form_open_multipart('produk/save'); ?>

			<input type="hidden" name="produk_id" id="produk_id" value="<?= $product['produk_id']; ?>">

			<table border="0">
				<tr>
					<td>Produk</td>
					<td>:</td>
					<td><input type="text" name="produk" id="produk" value="<?= $product['produk']; ?>" placeholder="Nama Produk"></td>
				</tr>

				<tr>
					<td>Deskripsi</td>
					<td>:</td>
					<td><textarea name="deskripsi" rows="10" cols="100" id="deskripsi"><?= $product['deskripsi']; ?></textarea></td>
				</tr>

				<tr>
					<td>Stok</td>
					<td>:</td>
					<td><input type="number" name="stok" id="stok" value="<?= $product['stok']; ?>" placeholder="Stok Produk"></td>
				</tr>

				<tr>
					<td>Foto</td>
					<td>:</td>
					<td><input type="file" name="foto" id="foto"></td>
				</tr>

				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td><img src="<?= base_url('assets/images/' . $product['foto']); ?>" width="150" height="100"></td>
				</tr>

				<tr>
					<td>Pemasok</td>
					<td>:</td>
					<td>
						<select name="supplier_id" id="supplier_id">
							<option value="">-- Pilih Pemasok --</option>
							
							<?php foreach ($suppliers as $supplier) : ?>
								
								<?php if($supplier->supplier_id == $product['supplier_id']) : ?>
									<option value="<?= $supplier->supplier_id; ?>" selected><?= $supplier->supplier; ?></option>
								<?php else : ?>
									<option value="<?= $supplier->supplier_id; ?>"><?= $supplier->supplier; ?></option>
								<?php endif; ?>	
							
							<?php endforeach; ?>

						</select>
					</td>
				</tr>

				<tr>
					<td>Kategori</td>
					<td>:</td>
					<td>
						<select name="kategori_id" id="kategori_id">
							<option value="">-- Pilih Kategori --</option>

							<?php foreach ($categories as $category) : ?>

								<?php if($category['kategori_id'] == $product['kategori_id']) : ?>
									<option value="<?= $category['kategori_id']; ?>" selected><?= $category['kategori']; ?></option>
								<?php else : ?>
									<option value="<?= $category['kategori_id']; ?>"><?= $category['kategori']; ?></option>
								<?php endif; ?>	

							<?php endforeach; ?>

						</select>
					</td>
				</tr>

				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td><input type="submit" name="btn-simpan" id="btn-simpan" value="Simpan"></td>
				</tr>


			</table>

		<?= form_close(); ?>

	</div>

	<?php $this->load->view('template/footer'); ?>

</div>

</body>
</html>