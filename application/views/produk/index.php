<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?= $title; ?></title>

	<?php $this->load->view('template/styles'); ?>

</head>

<body>

	<div id="container">
		<h1><?php echo $title; ?></h1>

		<div id="body">
			
			<code><button type="button" onclick="window.location.href = '<?= base_url('produk/tambah'); ?>';">Tambah Data</button></code>

			<?= $this->pagination->create_links(); ?>

			<table width="100%" border="1" cellpadding="0" cellspacing="0">

				<thead>
					<tr>
						<th>No.</th>
						<th>Produk</th>
						<th>Stok</th>
						<th>Foto</th>
						<th>Kategori Produk</th>
						<th>Pemasok</th>
						<th>Aksi</th>
					</tr>
				</thead>

				<tbody>
					
					<?php $no = $this->uri->segment(3) + 1; foreach ($products as $product) : ?>
						<tr>
							<td align="center"><?= $no; ?></td>
							<td ><?= $product['produk']; ?></td>
							<td align="center"><?= $product['stok']; ?></td>
							<td align="center"><img src="<?= base_url('assets/images/' . $product['foto']); ?>" width="150" height="100"/></td>
							<td><?= $product['kategori']; ?></td>
							<td><?= $product['supplier']; ?></td>
							<td><a href="<?= base_url('produk/ubah/' . $product['produk_id']); ?>">Ubah</a> <a href="<?= base_url('produk/hapus/' . $product['produk_id']); ?>" onclick="return confirm('Apakah anda yakin akan menghapus data ini?');">Hapus</a></td>
						</tr>
					<?php $no++; endforeach; ?>

				</tbody>

			</table>

			<?= $this->pagination->create_links(); ?>

		</div>

		<?php $this->load->view('template/footer'); ?>
		
	</div>

</body>
</html>