<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?= $title; ?></title>

	<?php $this->load->view('template/styles'); ?>

</head>
<body>

<div id="container">
	<h1><?php echo $title; ?></h1>

	<div id="body">

		<code><button type="button" onclick="window.location.href = '<?= base_url('produk'); ?>';">Kembali ke List Data</button></code>

		<?= validation_errors(); ?>

		<?= form_open_multipart('produk/save'); ?>

			<input type="hidden" name="produk_id" id="produk_id" value="">

			<table border="0">
				<tr>
					<td>Produk</td>
					<td>:</td>
					<td><input type="text" name="produk" id="produk" value="<?= set_value('produk'); ?>" placeholder="Nama Produk" required></td>
				</tr>

				<tr>
					<td>Deskripsi</td>
					<td>:</td>
					<td><textarea name="deskripsi" rows="10" cols="100" id="deskripsi" required><?= set_value('deskripsi'); ?></textarea></td>
				</tr>

				<tr>
					<td>Stok</td>
					<td>:</td>
					<td><input type="number" name="stok" id="stok" value="<?= set_value('stok'); ?>" placeholder="Stok Produk" required></td>
				</tr>

				<tr>
					<td>Foto</td>
					<td>:</td>
					<td><input type="file" name="foto" id="foto"></td>
				</tr>

				<tr>
					<td>Pemasok</td>
					<td>:</td>
					<td>
						<select name="supplier_id" id="supplier_id" required>
							<option value="">-- Pilih Pemasok --</option>
							<?php foreach ($suppliers as $supplier) : ?>
								<option value="<?= $supplier->supplier_id; ?>" <?= set_select('supplier_id', $supplier->supplier_id); ?>><?= $supplier->supplier; ?></option>
							<?php endforeach; ?>
						</select>
					</td>
				</tr>

				<tr>
					<td>Kategori</td>
					<td>:</td>
					<td>
						<select name="kategori_id" id="kategori_id" required>
							<option value="">-- Pilih Kategori --</option>
							<?php foreach ($categories as $category) : ?>
								<option value="<?= $category['kategori_id']; ?>" <?= set_select('kategori_id', $category['kategori_id']); ?>><?= $category['kategori']; ?></option>
							<?php endforeach; ?>
						</select>
					</td>
				</tr>

				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td><input type="submit" name="btn-simpan" id="btn-simpan" value="Simpan"></td>
				</tr>


			</table>

		<!-- </form> -->
		<?= form_close(); ?>

	</div>

	<?php $this->load->view('template/footer'); ?>

</div>

</body>
</html>